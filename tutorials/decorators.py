def benchmark(func):
    import time

    def wrapper():
        start = time.time()
        func()
        end = time.time()
        print('Время выполнения: {} секунд.'.format(end - start))

    return wrapper


@benchmark
def count():
    mylist = []
    for i in range(10000):
        mylist.append(i * 100 - 123456 + 7425 / 123)
    mylist.sort()


count()
print('*' * 10)


def my_decorator(f):
    def wrapped(*args, **kwargs):
        print('До функции')
        response = f(*args, **kwargs)
        print('После функции')
        return response

    print('Декорируем', f)
    return wrapped


@my_decorator
def my_function(a, b):
    print('В функции')
    return a + b


print(my_function(1, 2))
