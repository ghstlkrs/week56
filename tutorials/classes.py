class Vehicle:
    def print_details(self):
        print("Это родительский метод из класса Vehicle")


class Cycle(Vehicle):
    def print_details(self):
        print("Это дочерний метод из класса Cycle")


class Car:
    def __init__(self):
        print("Двигатель заведен")
        self.name = "corolla"
        self.__make = "toyota"
        self._model = 1999

    def print_details(self):
        print("Это дочерний метод из класса Car")

    @staticmethod
    def get_squares(a, b):
        return a * a, b * b

    def __str__(self):
        return "Car class Object"

    def start(self):
        print("Двигатель заведен")


print(Car.get_squares(3, 5))

car_aa = Car()
# Стоит упомянуть, что статические методы могут иметь
# доступ только к атрибутам класса в Python, вы не сможете обратиться к методам через self.
print(car_aa)
print('*' * 10)

car_a = Vehicle()
car_a.print_details()

car_b = Car()
car_b.print_details()

car_c = Cycle()
car_c.print_details()

print('*' * 10)


class Camera:
    def camera_method(self):
        print("Это родительский метод из класса Camera")


class Radio:
    def radio_method(self):
        print("Это родительский метод из класса Radio")


class CellPhone(Camera, Radio):
    def cell_phone_method(self):
        print("Это дочерний метод из класса CellPhone")


cell_phone_a = CellPhone()
cell_phone_a.camera_method()
cell_phone_a.radio_method()

print('*' * 10)


class Phone:
    username = "Kate"  # public variable
    __serial_number = "11.22.33"  # private variable
    __how_many_times_turned_on = 0  # private variable

    def call(self):  # public method
        print("Ring-ring!")

    def __turn_on(self):  # private method
        self.__how_many_times_turned_on += 1
        print("Times was turned on:", self.__how_many_times_turned_on)


my_phone = Phone()

my_phone._Phone__turn_on()
my_phone._Phone__serial_number = "44.55.66"
print("New serial number is", my_phone._Phone__serial_number)

print('*' * 10)


class Person:

    def __init__(self, name):
        self.__name = name  # имя человека

    @property
    def name(self):
        return self.__name

    def display_info(self):
        print(f"Name: {self.__name}")


class Employee(Person):

    def __init__(self, name, company):
        super().__init__(name)
        self.company = company

    def display_info(self):
        super().display_info()
        print(f"Company: {self.company}")

    def work(self):
        print(f"{self.name} works")


tom = Employee("Tom", "Microsoft")
tom.display_info()

print('*' * 10)


class Emp:

    def __init__(self):
        self.name = 'xyz'

        self.salary = 4000

    def show(self):
        print(self.name)

        print(self.salary)


e1 = Emp()
print("Dictionary form :", vars(e1))
print(dir(e1))
print('*' * 10)


class C:
    def __init__(self):
        self._x = None

    def getx(self):
        return self._x

    def setx(self, value):
        self._x = value

    def delx(self):
        del self._x

    x = property(getx, setx, delx, "I'm the 'x' property.")


c = C()
print(c.x)
c.x = 20
print(c.x)
del c.x


class Parrot:
    def __init__(self):
        self._voltage = 100000

    @property
    def voltage(self):
        """Get the current voltage."""
        return self._voltage
