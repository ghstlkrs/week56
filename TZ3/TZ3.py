from abc import abstractmethod
from pathlib import Path
import pandas as pd
from jinja2 import Environment, FileSystemLoader


class DataToHtmlConvertor:
    def __init__(self, file_path, html_path, town, **kwargs):
        self.file_path = file_path
        self.town = town.strip()
        self.html_path = html_path / f'{self.town}.html'
        self.dataframe = None

        self.need_open = True

        self.chunk_size = kwargs.get('chunk_size')
        self.new_line = kwargs.get('new_line')

    def processing(self):
        """
        Шаблон для обработки файла и превращения данных из него в html.
        :return: 
        """
        try:
            if self.need_open:
                with open(self.file_path, newline=self.new_line, encoding='utf-8') as file:
                    self.get_sorted_data(file)
            else:
                self.get_sorted_data(self.file_path)
        except Exception as exp:
            print(str(exp) + '\n')

        self.create_html()

    @abstractmethod
    def get_sorted_data(self, file):
        """
        Реализуется в дочерних классах обязательно.
        Читает и сортирует данные из файла.
        :param file:
        :return:
        """
        pass

    def create_html(self):
        """
        Создает файл html используя данные из dataframe.
        :return:
        """
        try:
            if self.dataframe is not None:
                with open(self.html_path, 'w', encoding='utf-8') as file:
                    self.dataframe.reset_index(drop=True, inplace=True)
                    headers = list(self.dataframe.columns.values)
                    rows = []
                    for i in range(len(self.dataframe)):
                        rows.append(self.dataframe.loc[i])
                    data = {'rows': rows, 'headers': headers, 'town': self.town}
                    template_env = Environment(loader=FileSystemLoader(str(Path.cwd().parent / 'templates')))
                    template = template_env.get_template('weather.html')
                    file.write(template.render(data))
            else:
                raise Exception('self.dataframe is None')
        except Exception as exp:
            print(str(exp) + '\n')


class CsvToHtml(DataToHtmlConvertor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if kwargs.get('new_line'):
            self.new_line = kwargs['new_line']
        else:
            self.new_line = ''

    def get_sorted_data(self, file):
        rows_list = []
        for chunk in pd.read_csv(file, chunksize=self.chunk_size):
            new_chunk = chunk[chunk.town == self.town]
            new_chunk.reset_index(drop=True, inplace=True)
            for i in range(len(new_chunk)):
                dict_row = new_chunk.loc[i].to_dict()
                rows_list.append(dict_row)
        df = pd.DataFrame(rows_list)
        sorted_df = df.sort_values(by=['day'])
        self.dataframe = sorted_df


class JsonToHtml(DataToHtmlConvertor):
    def get_sorted_data(self, file):
        df = pd.read_json(file)
        new_df = df[df.town == self.town]
        sorted_df = new_df.sort_values(by=['day'])
        self.dataframe = sorted_df


class XlsxToHtml(DataToHtmlConvertor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.need_open = False

    def get_sorted_data(self, file):
        df = pd.read_excel(file, engine='openpyxl')
        new_df = df[df.town == self.town]
        sorted_df = new_df.sort_values(by=['day'])
        self.dataframe = sorted_df


if __name__ == '__main__':
    CsvToHtml(
        file_path=Path(__file__).parent / 'weather.csv',
        html_path=Path.cwd(),
        town='Москва',
        chunk_size=10,
    ).processing()
    JsonToHtml(Path(__file__).parent / 'weather.json', Path.cwd(), 'Канберра').processing()
    XlsxToHtml(Path(__file__).parent / 'weather.xlsx', Path.cwd(), 'Стокгольм').processing()
    XlsxToHtml(Path(__file__).parent / 'weather.xlsx', Path.cwd(), 'Прага').processing()
    XlsxToHtml(Path(__file__).parent / 'weather.xlsx', Path.cwd(), 'Лондон').processing()

